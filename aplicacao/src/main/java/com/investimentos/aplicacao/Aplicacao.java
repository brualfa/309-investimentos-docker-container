package com.investimentos.aplicacao;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Aplicacao {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @NotNull
  private int produtoId;

  @NotNull
  private String clienteCpf;

  @Min(value = 0)
  private double saldo;

  @NotNull
  private LocalDate dataCriacao;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getProdutoId() {
    return produtoId;
  }

  public void setProdutoId(int produtoId) {
    this.produtoId = produtoId;
  }

  public String getClienteCpf() {
    return clienteCpf;
  }

  public void setClienteCpf(String clienteCpf) {
    this.clienteCpf = clienteCpf;
  }

  public double getSaldo() {
    return saldo;
  }

  public void setSaldo(double saldo) {
    this.saldo = saldo;
  }

  public LocalDate getDataCriacao() {
    return dataCriacao;
  }

  public void setDataCriacao(LocalDate dataCriacao) {
    this.dataCriacao = dataCriacao;
  }
}
