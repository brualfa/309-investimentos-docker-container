package com.investimentos.aplicacao;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClienteServiceIT {
  @Autowired
  private ClienteService subject;
  
  @Test
  public void deveBuscarUmProduto() {
    Optional<Cliente> optional = subject.consultar("123.123.123-12");
    
    assertTrue(optional.isPresent());
  }
}
